shufl
====================
Shuffle line or col, or both.

License
====================
MIT License

Usage
====================
	$ shufl [OPTION] [FILE]...

	Options:
		--col  -c    reverse col
		--line -l    reverse line
		--sep  -s    col separator
		--help       show this help message
		--version    print the version

Example
====================
	$ seq 1 5 | shufl
	1
	5
	3
	4
	2

	$ cat num.txt
	100,200,300
	400,500,600
	700,800,900

	$ cat num.txt | shufl -c
	,00130020,0
	0,0,0004065
	8009700,0,0

	$ cat num.txt | shufl -c -s=,
	200,100,300
	500,600,400
	700,800,900

	cat num.txt | shufl -c -l -s=,
	500,600,400
	300,200,100
	700,900,800

Author
====================
Kusabashira <kusabashira227@gmail.com>
