package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"math/rand"
	"os"
	"strings"
	"time"
)

func version() {
	os.Stderr.WriteString(`shuffle: v0.1.0
`)
}

func usage() {
	os.Stderr.WriteString(`
Usage: shuffle [OPTION] [FILE]...
Shuffle line or col, or both.

Options:
	--col  -c    reverse col
	--line -l    reverse line
	--sep  -s    col separator
	--help       show this help message
	--version    print the version

Report bugs to <kusabashira227@gmail.com>
`[1:])
}

func shuffleCol(r io.Reader, sep string) {
	rand.Seed(time.Now().UTC().UnixNano())

	reader := bufio.NewScanner(r)
	for reader.Scan() {
		line := reader.Text()
		spLine := strings.Split(line, sep)
		for cnt, i := range rand.Perm(len(spLine)) {
			os.Stdout.WriteString(spLine[i])
			if cnt < len(spLine) - 1 {
				os.Stdout.WriteString(sep)
			}
		}
		os.Stdout.WriteString("\n")
	}
}

func shuffleLine(r io.Reader) {
	rand.Seed(time.Now().UTC().UnixNano())

	lines := make([]string, 0, 128)
	reader := bufio.NewScanner(r)
	for reader.Scan() {
		lines = append(lines, reader.Text())
	}

	for _, i := range rand.Perm(len(lines)) {
		fmt.Println(lines[i])
	}
}

func shuffleColAndLine(r io.Reader, sep string) {
	rand.Seed(time.Now().UTC().UnixNano())

	lines := make([]string, 0, 128)
	reader := bufio.NewScanner(r)
	for reader.Scan() {
		lines = append(lines, reader.Text())
	}

	for _, li_i := range rand.Perm(len(lines)) {
		spLine := strings.Split(lines[li_i], sep)
		for cnt, co_i := range rand.Perm(len(spLine)) {
			os.Stdout.WriteString(spLine[co_i])
			if cnt < len(spLine) - 1 {
				os.Stdout.WriteString(sep)
			}
		}
		os.Stdout.WriteString("\n")
	}
}

func _main() (exitCode int, err error) {
	var isColMode, isLineMode bool
	var colSeparator string
	var isHelp, isVersion bool
	flag.BoolVar(&isColMode, "c", false, "reverse col")
	flag.BoolVar(&isColMode, "col", false, "reverse col")
	flag.BoolVar(&isLineMode, "l", false, "reverse line")
	flag.BoolVar(&isLineMode, "line", false, "reverse line")
	flag.StringVar(&colSeparator, "s", "", "col separator")
	flag.StringVar(&colSeparator, "sep", "", "col separator")
	flag.BoolVar(&isHelp, "help", false, "show this help message")
	flag.BoolVar(&isVersion, "version", false, "print the version")
	flag.Usage = usage
	flag.Parse()

	if isHelp {
		usage()
		return 0, nil
	}

	if isVersion {
		version()
		return 0, nil
	}

	var input io.Reader
	if flag.NArg() < 1 {
		input = os.Stdin
	} else {
		inputs := make([]io.Reader, 0, 8)
		for _, fname := range flag.Args() {
			in, err := os.Open(fname)
			if err != nil {
				return 1, err
			}
			defer in.Close()
			inputs = append(inputs, in)
		}
		input = io.MultiReader(inputs...)
	}

	switch {
	case isColMode && isLineMode:
		shuffleColAndLine(input, colSeparator)
	case isLineMode:
		shuffleLine(input)
	case isColMode:
		shuffleCol(input, colSeparator)
	default:
		shuffleLine(input)
	}

	return 0, nil
}

func main() {
	exitCode, err := _main()
	if err != nil {
		fmt.Fprintln(os.Stderr, "shuffle:", err)
	}
	os.Exit(exitCode)
}
